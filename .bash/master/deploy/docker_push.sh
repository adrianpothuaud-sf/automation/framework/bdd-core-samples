#!/bin/bash

########################################
# This script is part of the deploy phase of CI pipeline
########################################

echo ""
echo "Start Script .bash/master/deploy/docker_pull !"
echo ""
echo "*******************************"
echo ""
echo "         DOCKER_PUSH"
echo ""
echo "*******************************"
echo ""
echo "Push the Docker image from registry ..."
echo "-------------------------------"
echo ""

########################################
# here we push the docker image to Gitlab Registry
########################################
docker push $CI_REGISTRY_IMAGE:stable

echo ""
echo "End of Script !"
echo ""